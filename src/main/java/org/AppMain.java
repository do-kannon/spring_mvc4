package org;

import org.spring.AppConfig;
import org.spring.model.Personas;
import org.spring.service.PersonaService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import java.util.List;

/**
 * Created by dokannon on 21-04-17.
 */
public class AppMain {

        public static void main (String [] args){

            AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

            PersonaService personaService =  (PersonaService) context.getBean("personasService");

            Personas personas = new Personas();

            personas.setNombres("andres");
            personas.setApellidos("navarro");
            personas.setEdad(34);

            personaService.savePersona(personas);

            List<Personas> listPersonas = personaService.findAllPersonas();

            for (Personas aux : listPersonas){
                System.out.println("Datos Personas : " +aux.getNombres());
            }


            for (Personas aux : listPersonas){
                personaService.deletePersonaByNombres(aux.getNombres());
            }
            context.close();
        }
}
