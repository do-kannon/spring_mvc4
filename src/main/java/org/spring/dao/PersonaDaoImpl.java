package org.spring.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.spring.model.Personas;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dokannon on 20-04-17.
 */

@Repository("personaDAO")
public class PersonaDaoImpl extends AbstractDAO<Personas> implements PersonaDAO{


    public PersonaDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void savePersona(Personas personas) {
        persist(personas);
    }

    @Override
    public List<Personas> findAllPersonas() {
      return (List<Personas>) getSession().createCriteria(Personas.class).list();
    }

    @Override
    public void deletePersonaByNombres(String nombres) {
        Query query = getSession().createSQLQuery("delete from PERSONAS where idPERSONAS = :nombres");
        query.setString("nombres", nombres);
        query.executeUpdate();

    }

    @Override
    public void updatePersona(Personas personas) {
        getSession().update(personas);

    }

    @Override
    public List<Personas> findByNombre(String nombres) {

        return (List<Personas>)getSession().createCriteria(Personas.class,nombres);
    }
}
