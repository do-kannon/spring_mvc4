package org.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by dokannon on 20-04-17.
 */
public abstract class AbstractDAO<T> {

    private SessionFactory sessionFactory;

    @Autowired
    public AbstractDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession(){

        return sessionFactory.getCurrentSession();
    }

    public void persist (T entity){
        getSession().save(entity);

    }

    public void delete (T entity){
        getSession().delete(entity);
    }

}
