package org.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by dokannon on 20-04-17.
 */

@Configuration
@ComponentScan(basePackages = {"org.spring"})
public class AppConfig {


    public static void main(String... args) {

        Shape s = () -> System.out.println("Fill");
        s.fill();

    }

    public interface Shape {

        void fill();

    }

}
