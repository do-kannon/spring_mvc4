package org.spring.service;

import org.spring.model.Personas;

import java.util.List;

/**
 * Created by dokannon on 20-04-17.
 */
public interface PersonaService {

    void savePersona (Personas personas);

    List<Personas> findAllPersonas ();

    void deletePersonaByNombres(String nombres);

    void updatePersona (Personas personas);

    List<Personas> findByNombre(String nombres);
}
