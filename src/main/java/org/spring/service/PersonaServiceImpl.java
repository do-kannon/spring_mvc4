package org.spring.service;

import org.spring.dao.PersonaDAO;
import org.spring.model.Personas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by dokannon on 20-04-17.
 */
@Service("personasService")
@Transactional
public class PersonaServiceImpl implements PersonaService{

    @Autowired
    PersonaDAO personaDAO;

    @Override
    public void savePersona(Personas personas) {
        personaDAO.savePersona(personas);
    }

    @Override
    public List<Personas> findAllPersonas() {
        return personaDAO.findAllPersonas();
    }

    @Override
    public void deletePersonaByNombres(String nombres) {
        personaDAO.deletePersonaByNombres(nombres);
    }

    @Override
    public void updatePersona(Personas personas) {
        personaDAO.updatePersona(personas);
    }

    @Override
    public List<Personas> findByNombre(String nombres) {
       return personaDAO.findByNombre(nombres);
    }
}
