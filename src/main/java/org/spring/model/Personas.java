package org.spring.model;

import javax.persistence.*;

/**
 * Created by dokannon on 20-04-17.
 */

@Entity
@Table(name="PERSONAS")
public class Personas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="idPERSONAS")
    private int idPersonas;

    @Column(name="NOMBRES",nullable = true)
    private String nombres;

    @Column(name="APELLIDOS",nullable = true)
    private String apellidos;

    @Column(name="EDAD" , nullable = true)
    private int edad;


    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getIdPersonas() {
        return idPersonas;
    }

    public void setIdPersonas(int idPersonas) {
        this.idPersonas = idPersonas;
    }


}
